﻿#include <iostream>
#include <SFML/Graphics.hpp>
#include <vector>

int w = 900, h = 900;

sf::RenderWindow window(sf::VideoMode(w, h), "tic_tac_toe");

void draw(const std::string& s, const int& X, const int& Y) {

    sf::Image image;
    image.loadFromFile(s);

    sf::Texture texture;
    texture.loadFromImage(image);

    sf::Sprite sprite;
    sprite.setTexture(texture);
    sprite.setPosition(X, Y);

    window.draw(sprite);
}

void draw_everything(const std::vector<std::vector<int>>& c) {

    int X = 0, Y = 0;

    draw("images/tic_tac_toe_field.jpg", X, Y);

    for (int i = 0; i < 3; i++) {

        for (int j = 0; j < 3; j++) {

            if (c[i][j]) {

                X = (2 * j + 1) * (w / 3) / 2 - 100;
                Y = (2 * i + 1) * (h / 3) / 2 - 100;

                if (c[i][j] == 1) {

                    draw("images/tic_tac_toe_crosses.jpg", X, Y);   // 200x200
                }
                else {

                    draw("images/tic_tac_toe_noughts.jpg", X, Y);   // 200x200
                }
            }
        }
    }
}

void raiting1(std::vector<int>& r, std::string& first_player, std::string& second_player, int& game_over) {

    int difference = first_player.length() - second_player.length();

    if (difference >= 0) {

        std::cout << std::endl << "raiting" << std::endl;
        std::cout << std::endl << "player " << first_player << "   " << r[0];
        std::cout << std::endl << "player " << second_player << "   ";

        for (int i = 0; i < difference; i++) {

            std::cout << " ";
        }

        std::cout << r[1] << std::endl;
    }
    else {

        std::cout << std::endl << "raiting" << std::endl;
        std::cout << std::endl << "player " << first_player << "   ";

        for (int i = difference; i < 0; i++) {

            std::cout << " ";
        }

        std::cout << r[0];
        std::cout << std::endl << "player " << second_player << "   " << r[1] << std::endl;
    }
}

void press(std::vector<std::vector<int>>& c, int& player, int& game_over, std::vector<int>& r, std::string& first_player, std::string& second_player, sf::Event event) {

    int pressed = 0;
    if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G) {
        raiting1(r, first_player, second_player, game_over);
    }
    if (game_over) {

        return;
    }

    if (event.type == sf::Event::MouseButtonPressed && event.mouseButton.button == sf::Mouse::Left && !pressed) {

        int X = event.mouseButton.x;
        int Y = event.mouseButton.y;

        if (Y < (h / 3)) {

            if (X < (w / 3) && !c[0][0]) {

                c[0][0] = player;
                pressed = 1;
            }
            else if (X > (w / 3.) && X < (w / 3 * 2) && !c[0][1]) {

                c[0][1] = player;
                pressed = 1;
            }
            else if (X > (w / 3 * 2) && !c[0][2]) {

                c[0][2] = player;
                pressed = 1;
            }

        }
        else if (Y < (h / 3 * 2)) {

            if (X < (w / 3) && !c[1][0]) {

                c[1][0] = player;
                pressed = 1;
            }
            else if (X > (w / 3) && X < (w / 3 * 2) && !c[1][1]) {

                c[1][1] = player;
                pressed = 1;
            }
            else if (X > (w / 3 * 2) && !c[1][2]) {

                c[1][2] = player;
                pressed = 1;
            }

        }
        else {

            if (X < (w / 3) && !c[2][0]) {

                c[2][0] = player;
                pressed = 1;
            }
            else if (X > (w / 3) && X < (w / 3 * 2) && !c[2][1]) {

                c[2][1] = player;
                pressed = 1;
            }
            else if (X > (w / 3 * 2) && !c[2][2]) {

                c[2][2] = player;
                pressed = 1;
            }

        }
    }

    if (pressed) {

        player = player % 2 + 1;
    }
}

void check_win(const std::vector<std::vector<int>>& c, std::vector<int>& r, const std::string& first_player, const std::string& second_player, int& game_over) {
    
    int check = 0;

    if (game_over) {

        return;
    }

    if (c[0][0] == c[1][1] && c[1][1] == c[2][2] && c[0][0] == 1 ||
        c[2][0] == c[1][1] && c[1][1] == c[0][2] && c[2][0] == 1) {

        check = 1;
    }
    else if (c[0][0] == c[1][1] && c[1][1] == c[2][2] && c[0][0] == 2 ||
             c[2][0] == c[1][1] && c[1][1] == c[0][2] && c[2][0] == 2) {

        check = 2;
    }

    for (int i = 0; i < 3; i++) {

        if (c[i][0] == c[i][1] && c[i][1] == c[i][2] && c[i][0] == 1 ||
            c[0][i] == c[1][i] && c[1][i] == c[2][i] && c[0][i] == 1) {

            check = 1;
        }
        else if (c[i][0] == c[i][1] && c[i][1] == c[i][2] && c[i][0] == 2 ||
                 c[0][i] == c[1][i] && c[1][i] == c[2][i] && c[0][i] == 2) {

            check = 2;
        }
    }

    if (check == 1) {

        r[0] += 1;
        std::cout << std::endl << "player " << first_player << " won";
        std::cout << std::endl << "player " << second_player << " lost" << std::endl;
        game_over = 1;
    }
    else if (check == 2) {

        r[1] += 1;
        std::cout << std::endl << "player " << second_player << " won";
        std::cout << std::endl << "player " << first_player << " lost" << std::endl;
        game_over = 1;
    }
}

void check_tie(const std::vector<std::vector<int>>& c, const std::string& first_player, const std::string& second_player, int& game_over) {

    int check = 0;

    if (game_over) {

        return;
    }

    if ((c[0][0] != c[1][1] && c[0][0] != 0 && c[1][1] != 0 ||
         c[0][0] != c[2][2] && c[0][0] != 0 && c[2][2] != 0 ||
         c[1][1] != c[2][2] && c[1][1] != 0 && c[2][2] != 0) &&
        (c[2][0] != c[1][1] && c[2][0] != 0 && c[1][1] != 0 ||
         c[2][0] != c[0][2] && c[2][0] != 0 && c[0][2] != 0 ||
         c[1][1] != c[0][2] && c[1][1] != 0 && c[0][2] != 0)) {

        check = 1;
    }

    for (int i = 0; i < 3; i++) {

        if (check &&
            (c[i][0] != c[i][1] && c[i][0] != 0 && c[i][1] != 0 ||
             c[i][0] != c[i][2] && c[i][0] != 0 && c[i][2] != 0 ||
             c[i][1] != c[i][2] && c[i][1] != 0 && c[i][2] != 0) &&
            (c[0][i] != c[1][i] && c[0][i] != 0 && c[1][i] != 0 ||
             c[0][i] != c[2][i] && c[0][i] != 0 && c[2][i] != 0 ||
             c[1][i] != c[2][i] && c[1][i] != 0 && c[2][i] != 0)) {

        }
        else {

            check = 0;
        }
    }

    if (check) {

        std::cout << std::endl << "player " << first_player << " and " << "player " << second_player << " tied" << std::endl;
        game_over = 1;
    }
}

void next_round(std::vector<std::vector<int>>& c, int& starting_player, int& player, int& game_over, int& round) {

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space) && game_over) {

        for (int i = 0; i < 3; i++) {

            for (int j = 0; j < 3; j++) {

                c[i][j] = 0;
            }
        }

        game_over = 0;
        round += 1;
        player = starting_player % 2 + 1;
        starting_player = player;
        
        std::cout << "=========================================";
        std::cout << std::endl << "round " << round << std::endl;
    }
}

void raiting(std::vector<int>& r, std::string& first_player, std::string& second_player, int& game_over) {
    
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R) && game_over == 1) {

        int difference = first_player.length() - second_player.length();

        game_over = -1;

        if (difference >= 0) {

            std::cout << std::endl << "raiting" << std::endl;
            std::cout << std::endl << "player " << first_player << "   " << r[0];
            std::cout << std::endl << "player " << second_player << "   ";

            for (int i = 0; i < difference; i++) {

                std::cout << " ";
            }

            std::cout << r[1] << std::endl;
        }
        else {

            std::cout << std::endl << "raiting" << std::endl;
            std::cout << std::endl << "player " << first_player << "   ";

            for (int i = difference; i < 0; i++) {

                std::cout << " ";
            }

            std::cout << r[0];
            std::cout << std::endl << "player " << second_player << "   " << r[1] << std::endl;
        }
    }
}

int main()
{
    int starting_player = 1, player = 1, game_over = 0, round = 1;
    std::string first_player, second_player;
    std::vector<int> r(2, 0);
    std::vector<std::vector<int>> c(3, std::vector<int>(3, 0));
    
    std::cout << "please enter your nicknames" << std::endl;
    
    std::cin >> first_player >> second_player;

    std::cout << "=========================================";
    std::cout << std::endl << "round " << round << std::endl;

    while (window.isOpen()) {
       
        sf::Event event;

        while (window.pollEvent(event)) {


            press(c, player, game_over, r, first_player, second_player, event);

            if (event.type == sf::Event::Closed) {

                window.close();
            }
        }

        window.clear();
        draw_everything(c);
        check_win(c, r, first_player, second_player, game_over);
        check_tie(c, first_player, second_player, game_over);
        next_round(c, starting_player, player, game_over, round);
        raiting(r, first_player, second_player, game_over);
        window.display();
    }

    return 0;
}
